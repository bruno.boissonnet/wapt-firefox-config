# -*- coding: UTF-8 -*-
from setuphelpers import *
import json
import platform

uninstallkey = []

# Installation procedure: https://support.mozilla.org/kb/customizing-firefox-using-policiesjson
# https://klaus-hartnegg.de/gpo/2018-05-26-Firefox-Policies.html
# Use Add-ons Enterprise Policy Generator to create policies.json an copy on package folder:
# https://addons.mozilla.org/firefox/addon/enterprise-policy-generator/

# Defining variables
app_name ='Mozilla Firefox'
policies_file = 'policies.json'


def install():
    # Installing the package
    for ffox in installed_softwares(name=app_name):
        # Initializing variables
        ffox_dir = ffox['install_location']
        ffox_dist_dir = makepath(ffox_dir, 'distribution')
        policies_path = makepath(ffox_dist_dir, policies_file)

        killalltasks(control.impacted_process.split(','))

        # Create distribution folder if not present
        if not isdir(ffox_dist_dir):
            mkdirs(ffox_dist_dir)

        # Forcing package Firefox configuration
        if isfile(policies_path):
            remove_file(policies_path)

        # Copy Firefox configuration
        filecopyto(policies_file, policies_path)

        # Removing policies from registry since it bypass the policies.json file (source: https://support.mozilla.org/bm/questions/1236197)
        ffox_policies_reg_path = r'SOFTWARE\Policies\Mozilla\Firefox'
        if reg_key_exists(HKEY_LOCAL_MACHINE, ffox_policies_reg_path):
            registry_deletekey(HKEY_LOCAL_MACHINE, ffox_policies_reg_path, '', force=True)

def uninstall():
    # Uninstalling the package
    for ffox in installed_softwares(name=app_name):
        # Initializing variables
        ffox_dir = ffox['install_location']
        ffox_dist_dir = makepath(ffox_dir, 'distribution')
        policies_path = makepath(ffox_dist_dir, policies_file)

        # Removing Firefox configuration
        if isfile(policies_path):
            remove_file(policies_path)

        # Adding Minimal configuration for Firefox you may use the package "tis-config-firefox" for further options
        if not isfile(policies_path):
            registry_set(HKEY_LOCAL_MACHINE, r'SOFTWARE\Policies\Mozilla\Firefox', 'DisableAppUpdate', 1, REG_DWORD)
            registry_set(HKEY_LOCAL_MACHINE, r'SOFTWARE\Policies\Mozilla\Firefox', 'DisableTelemetry', 1, REG_DWORD)

def audit():
    # Verifying that the configuration is applied
    for ffox in installed_softwares(name=app_name):
        # Initializing variables
        ffox_dir = ffox['install_location']
        ffox_dist_dir = makepath(ffox_dir, 'distribution')
        policies_path = makepath(ffox_dist_dir, policies_file)

        if not isfile(policies_path):
            print("WARNING: Configuration is NOT applied for %s, Re-applying" % ffox['name'])
            install()
            return "WARNING"
        else:
            print("OK: Configuration is correctly applied for %s" % ffox['name'])
            return "OK"

    print("WARNING: Firefox is NOT installed")
    return "WARNING"

def update_package():
    pass



