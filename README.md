# GTDA-WAPT-Firefox_config

Paquet [WAPT](https://www.tranquil.it/gerer-parc-informatique/decouvrir-wapt/) pour l'installation silencieuse de la configuration de Firefox pour INRAE.

*Dépôt du projet Mozilla Firefox policies : https://github.com/mozilla/policy-templates/releases?page=5*

## Paquet Config Firefox

👉 Dernière version de la configuration : 115.1.0-3

Ce paquet installe un fichier de configuration pour Firefox de manière silencieuse.

- Ajout d'un dossier "Marque-pages INRAE" non effaçable dans la barre personnelle
- Ce dossier contient les liens suivants :
    + #TEMPS : https://temps-activites.inrae.fr/fr/
    + ARIANE : https://ariane.inrae.fr/block/
    + Outlook Web App : https://messagerie.inrae.fr/
    + GRR (Gestion des Réservations de Ressources) : https://w4.paca.inrae.fr/intranet/grr/index.html
    + Réservation VS mutualisés : https://reservations-vehicules.inrae.fr
    + WIFI INRAE : https://intranet.paca.inrae.fr/Appui-a-la-recherche/Informatique-de-centre/Acces-et-service/wifi
    + RH Agent : https://rh-production.s2i.inra.fr/hra-space/saml
    + Finances S2I Gestion : https://finances.s2i.inra.fr/psp/PROD/?cmd=login
    + RESELEC (Ressources Editoriales éLECtroniques) : https://www6.inrae.fr/reselec/
    + Création de sondages (LimeSurvey) : https://sondages.intranet.inrae.fr/index.php/admin
    + Annuaire Téléphonique INRAE : https://intranet.annuaire.inrae.fr
    + Les départements de recherche INRA : https://intranet.inrae.fr/national/les-departements
    + Dossier COMMUNICATION :
        * Skype Entreprise Web Scheduler : https://scheduler.inrae.fr/
        * Zoom : https://inrae-fr.zoom.us/
        * BigBlueButton : https://bbb.visio.inrae.fr/
        * Rendez-Vous - RENATER : https://www.renater.fr/services/collaborer-simplement/rendez-vous/
        * Tchap : https://www.tchap.gouv.fr/#/welcome
    + Dossier DONNÉES :
        * HAL Archive Ouverte d'INRAE : https://hal.inrae.fr/
        * Portail des ressources éditoriales électroniques (RESELEC) : https://www6.inrae.fr/reselec/Aides/Comment-acceder-aux-ressources
        * Bases de données RESELEC : https://www6.inrae.fr/reselec/Bases-de-donnees
        * GitLab INRAE : https://forgemia.inra.fr/users/sign_in
    + Dossier RENATER :
        * Accueil | RENATER : https://www.renater.fr/
        * FileSender 2.0 : https://filesender.renater.fr/index.php?s=home
        * RENAvisio : https://renavisio.renater.fr/
        * Evento : https://evento.renater.fr/
        * RENDEZ-VOUS : https://sourcesup.renater.fr/nuxeo/ui/#!/home
        * Accueil - Nuxeo Platform : https://sourcesup.renater.fr/nuxeo/ui/#!/home
        * eduroam : https://www.eduroam.fr/
    + Dossier SHAREPOINT INRAE :
        * NATIONAL : https://sites.inrae.fr/
        * PROCÉDURES INFORMATIQUES : https://partage-fichiers.inrae.fr/perso/bboissonnet/Informatique/
    + Dossier FORMATION :
        * PLATEFORME e-FORMATION et RESSOURCES MULTIMEDIA : https://elearning.formation-permanente.inrae.fr/
        * Cours : Messagerie et Agenda : https://elearning.formation-permanente.inrae.fr/course/view.php?id=289&section=0
        * Cours : Télétravail : https://elearning.formation-permanente.inrae.fr/course/index.php?categoryid=195
        * Cours : Zotero : https://ist.inrae.fr/produit/zotero_formation/
    + Dossier INTERNET INRAE :
        * INRAE : https://www.inrae.fr
        * PACA : https://www.inrae.fr/centres/provence-alpes-cote-dazur
        * INRAE JOBS : https://jobs.inrae.fr/
    + Dossier INTRANET INRAE :
        * National : https://intranet.inrae.fr/national/
        * PACA : https://intranet.paca.inrae.fr/
        * PACA PRATIQUE : https://intranet.paca.inrae.fr/PACA-pratique/Infos-utiles-a-tous-les-agents
        * Charte identitaire : https://intranet.inrae.fr/charte-identitaire/
        * Kit de communication : https://intranet.inrae.fr/communication/Outils-de-com/Kit-de-communication
        * Abécédaire INRAE : https://intranet.inra.fr/national/nouvel-institut-inrae/abecedaire-inrae-1725
        * Sigles INRAE : https://intranet.inrae.fr/national/vie-de-linstitut/sigles-inrae-5248?ticket=ST-2320781-ULeoPbHwQ5mR445qpy6N-cas
        * Chambres de stagiaires : https://intranet.paca.inrae.fr/PACA-pratique/Chambres
        * Cybersécurité : https://intranet.inrae.fr/cybersecurite
- Mises à jour désactivées
- Désactivation de l'agent Mozilla (Windows seulement)
- Compte Firefox activé
- Télémétrie désactivée
- Affichage de la barre de menu
- Affichage de la barre personnelle (Toujours affichée)
- Vérification navigateur par défaut désactivée
- Page d'accueil de Firefox :
    + Brèves : désactivée
    + Recherche web : activée
    + Topsites : activée
    + Snippets : désactivée
    + Pocket : désactivée
- Page nouvel onglet : activée
- Les signets et les signets par défaut (les balises les plus visitées, les plus récentes) ne sont pas créés.
- Blocage des fenêtres popup : 
    + Activée
    + Exceptions :
        * https://finances.s2i.inra.fr/
- Barre de recherche : unifiée
- Suggestions de recherche : activée
- Affichage du bouton Accueil
- Menu Aide :
    + Ajout :
        * Service informatique : https://partage-fichiers.inra.fr/perso/bboissonnet/Informatique/Mes%20pages/Accueil.aspx




## Utiliser ce paquet

Pour utiliser ce paquet, il faut : 

1. Télécharger tous les fichiers ici : https://forgemia.inra.fr/bruno.boissonnet/wapt-firefox-config/-/archive/main/wapt-firefox-config-main.zip
2. Décompresser l'archive **wapt-firefox-config-main.zip**
3. Ouvrir la console WAPT
3. Aller dans le sous-menu **Outils > Construire et importer un paquet dans le dépôt**
4. Pointer sur le dossier **wapt-firefox-config-main/wapt-firefox-config-main** pour importer le paquet

À chaque nouvelle version de Firefox/Firefox ESR, il faut vérifier si de nouveaux paramètres ont été ajoutés ou d'anciens paramètres ont été modifiés ici : [Dépôt du projet Mozilla Firefox policies](https://github.com/mozilla/policy-templates/releases?page=5).


## Notes de version

### 115.1.0-3

Ajout du lien vers la page des départements de recherche INRAE (https://intranet.inrae.fr/national/les-departements)

#### FICHIER : policies.json

- ManagedBookmarks : 
  + AJOUT       : 
  ```json
  {
    "url": "https://intranet.inrae.fr/national/les-departements",
    "name": "Les départements de recherche INRAE"
  },
  ```

### 115.1.0

#### FICHIER : setup.py

- Nouvelle version du paquet qui correspond à la dernière version de Firefox ESR installée : 115.1.0

#### FICHIER : policies.json

- ManagedBookmarks : 
  + SUPPRESSION : 
  ```json
  {
    "url": "https://pepiniere.inrae.fr/",
    "name": "La Pépinière Numérique"
  },
  ```
  + AJOUT       : 
  ```json
  {
    "url": "https://intranet.paca.inrae.fr/Appui-a-la-recherche/Informatique-de-centre/Acces-et-service/wifi",
    "name": "WIFI INRAE"
  },
  ```
- AJOUT        :  
```json
"DisableDefaultBrowserAgent": true,
```
- MODIFICATION :  
```json
"DisplayMenuBar": "default-on",
```
- AJOUT        :  
```json
"DisplayBookmarksToolbar": "always",
```
- AJOUT        :  
```json
"ShowHomeButton": true,
```